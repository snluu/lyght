﻿namespace Lyght
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboColor = new System.Windows.Forms.ComboBox();
            this.chkBlink = new System.Windows.Forms.CheckBox();
            this.cmdSetupLync = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cboColor
            // 
            this.cboColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboColor.FormattingEnabled = true;
            this.cboColor.Items.AddRange(new object[] {
            "[NONE]",
            "RED",
            "YELLOW",
            "GREEN"});
            this.cboColor.Location = new System.Drawing.Point(12, 12);
            this.cboColor.Name = "cboColor";
            this.cboColor.Size = new System.Drawing.Size(121, 21);
            this.cboColor.TabIndex = 1;
            this.cboColor.SelectedIndexChanged += new System.EventHandler(this.cboColor_SelectedIndexChanged);
            // 
            // chkBlink
            // 
            this.chkBlink.AutoSize = true;
            this.chkBlink.Location = new System.Drawing.Point(139, 14);
            this.chkBlink.Name = "chkBlink";
            this.chkBlink.Size = new System.Drawing.Size(49, 17);
            this.chkBlink.TabIndex = 2;
            this.chkBlink.Text = "Blink";
            this.chkBlink.UseVisualStyleBackColor = true;
            // 
            // cmdSetupLync
            // 
            this.cmdSetupLync.Location = new System.Drawing.Point(12, 39);
            this.cmdSetupLync.Name = "cmdSetupLync";
            this.cmdSetupLync.Size = new System.Drawing.Size(75, 23);
            this.cmdSetupLync.TabIndex = 3;
            this.cmdSetupLync.Text = "Setup Lync";
            this.cmdSetupLync.UseVisualStyleBackColor = true;
            this.cmdSetupLync.Click += new System.EventHandler(this.cmdSetupLync_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 261);
            this.Controls.Add(this.cmdSetupLync);
            this.Controls.Add(this.chkBlink);
            this.Controls.Add(this.cboColor);
            this.Name = "MainForm";
            this.Text = "Lyght";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboColor;
        private System.Windows.Forms.CheckBox chkBlink;
        private System.Windows.Forms.Button cmdSetupLync;
    }
}

