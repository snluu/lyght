﻿using CommandMessenger;
using CommandMessenger.Serialport;
using System;
using System.Configuration;

namespace Lyght
{
    public class Serial : IDisposable
    {
        private SerialTransport serialTransport;
        private CmdMessenger cmdMessenger;

        public void Setup()
        {
            this.serialTransport = new SerialTransport();
            this.serialTransport.CurrentSerialSettings.PortName = ConfigurationManager.AppSettings["ComPort"];
            this.serialTransport.CurrentSerialSettings.BaudRate = 115200;
            this.serialTransport.CurrentSerialSettings.DtrEnable = false;

            this.cmdMessenger = new CmdMessenger(this.serialTransport);
            this.cmdMessenger.BoardType = BoardType.Bit16; // Arduino Uno
            bool x = this.cmdMessenger.Connect();
        }

        public void SetLed(LedColor color, bool blink)
        {
            int colorValue = (int)color;
            if (blink)
            {
                colorValue += 20;
            }

            SendCommand cmd = new SendCommand((int)SerialCommand.SetLed, colorValue);
            this.cmdMessenger.SendCommand(cmd);
        }

        public void Dispose()
        {
            if (this.cmdMessenger != null)
            {
                this.cmdMessenger.Dispose();
            }

            if (this.serialTransport != null)
            {
                this.serialTransport.Dispose();
            }
        }
    }
}
