﻿using Lyncx;
using Microsoft.Lync.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lyght
{
    public partial class MainForm : Form
    {
        private Serial serial;
        private LyncxClient lyncxClient;

        public MainForm()
        {
            InitializeComponent();
            this.serial = new Serial();
            this.serial.Setup();

            this.lyncxClient = new LyncxClient();
            this.lyncxClient.AvailabilityChanged += lyncxClient_AvailabilityChanged;
            this.lyncxClient.Setup();
        }

        void lyncxClient_AvailabilityChanged(object sender, AvailabilityChangedEventArgs e)
        {
            bool blink = false;
            LedColor ledColor = LedColor.Yellow;

            switch (e.Availability)
            {
                case ContactAvailability.Free:
                    ledColor = LedColor.Green;
                    break;
                case ContactAvailability.Busy:
                case ContactAvailability.DoNotDisturb:
                    ledColor = LedColor.Red;
                    break;
                default:
                    ledColor = LedColor.Yellow;
                    break;
            }

            if (e.Availability == ContactAvailability.DoNotDisturb)
            {
                blink = true;
            }

            this.serial.SetLed(ledColor, blink);
        }

        private void cboColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboColor.SelectedIndex == 0)
            {
                this.serial.SetLed(LedColor.None, chkBlink.Checked);
            }
            else
            {
                this.serial.SetLed((LedColor)(cboColor.SelectedIndex + 8), chkBlink.Checked);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.serial != null)
            {
                this.serial.SetLed(LedColor.None, blink: false);
                this.serial.Dispose();
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void cmdSetupLync_Click(object sender, EventArgs e)
        {
            this.lyncxClient.Setup();
        }
    }
}
