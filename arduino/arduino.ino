#include <CmdMessenger.h>

const int RED_LED = 9;
const int YELLOW_LED = 10;
const int GREEN_LED = 11;
const unsigned long BLINK_DURATION = 750L;

CmdMessenger cmdMessenger = CmdMessenger(Serial);
int activeLed;
int ledChanged;
int blinkLed;
int ledOn;
unsigned long lastBlink;

enum
{
  kSetLed = 0, // command to set LED state
};

void attachCommandCallbacks()
{
  cmdMessenger.attach(kSetLed, onSetLed);
}

void onSetLed()
{
  activeLed = cmdMessenger.readInt32Arg();
  
  // Serial.print("active led: ");
  // Serial.println(activeLed);
  
  if (activeLed > 20)
  {
    activeLed -= 20;
    blinkLed = 1;
  }
  else
  {
    blinkLed = 0;
  }
  
  // Serial.print("blink Led: ");
  // Serial.println(blinkLed);
  
  ledChanged = 1;
}

void switchLeds()
{
  analogWrite(RED_LED, activeLed == RED_LED ? 255 : 0);
  analogWrite(YELLOW_LED, activeLed == YELLOW_LED ? 255 : 0);
  analogWrite(GREEN_LED, activeLed == GREEN_LED ? 255 : 0);
  
  if (activeLed == RED_LED || activeLed == YELLOW_LED || activeLed == GREEN_LED)
  {
    ledOn = 1;
  }
  
  ledChanged = 0;
}

void toggleActiveLed()
{
  unsigned long currentMillis = millis();
  
  if (currentMillis - lastBlink < BLINK_DURATION)
  {
    return;
  }
  
  lastBlink = currentMillis;
  
  if (ledOn)
  {
    // turn off LED
    analogWrite(activeLed, 0);
    // Serial.print("turn off at ");
    ledOn = 0;
  }
  else
  {
    // turn on LED
    analogWrite(activeLed, 255);
    // Serial.print("turn on at ");
    ledOn = 1;
  }
  
  // Serial.println(lastBlink);
}

void setup()
{  
  ledChanged = 0;
  activeLed = 0;
  blinkLed = 0;
  lastBlink = 0L;
  
  // Listen on the serial connection for messages from the PC
  // 115200 is the max speed for Arduino Uno
  Serial.begin(115200);
  
  // add a new line to every command
  cmdMessenger.printLfCr();
  
  attachCommandCallbacks();
  
  pinMode(RED_LED, OUTPUT);
  pinMode(YELLOW_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
}

void loop()
{
  cmdMessenger.feedinSerialData();
  
  if (ledChanged)
  {
    switchLeds();
  }
  
  if (activeLed && blinkLed)
  {    
    toggleActiveLed();
  }
}
